import AuthLogo from "./extensions/vi.png";
import MenuLogo from "./extensions/menu.png";
import Favicon from "./extensions/favicon.ico";

export default {
  config: {
    locales: [
      // 'ar',
      // 'fr',
      // 'cs',
      // 'de',
      // 'dk',
      // 'es',
      // 'he',
      // 'id',
      // 'it',
      // 'ja',
      // 'ko',
      // 'ms',
      // 'nl',
      // 'no',
      // 'pl',
      // 'pt-BR',
      // 'pt',
      // 'ru',
      // 'sk',
      // 'sv',
      // 'th',
      // 'tr',
      // 'uk',
      // 'vi',
      // 'zh-Hans',
      // 'zh',
    ],
    auth: {
      logo: AuthLogo,
    },
    head: {
      favicon: Favicon,
    },
    menu: {
      logo: MenuLogo,
    },
    theme: {
      light: {
        colors: {
          primary100: "#EAFBE7",
          primary200: "#C6F1C3",
          primary500: "#5CB176",
          primary600: "#338148",
          primary700: "#2E6847",
          danger700: "#B72B1A",
        },
      },
      dark: {
        colors: {
          neutral150: "#EBEBEF",
          neutral200: "#DCDDE5",
          neutral300: "#C0C1CE",
          neutral400: "#A4A5BB",
          neutral600: "#666687",
          danger700: "#B72B1A",
        },
      },
    },
    tutorials: false,
    notifications: { releases: false },
  },
  bootstrap(app) {
    console.log(app);
  },
};
