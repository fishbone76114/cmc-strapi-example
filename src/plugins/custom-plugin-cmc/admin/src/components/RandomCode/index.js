import React, { useState } from "react";
import {
  Button,
  Field,
  FieldInput,
  FieldLabel,
  Stack,
} from "@strapi/design-system";

const generateRandomCode = () => {
  const chars =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  let password = "";
  for (let i = 0; i < 16; i++) {
    password += chars[Math.floor(Math.random() * chars.length)];
  }
  return password;
};

const RandomCode = ({ name, value, onChange }) => {
  const [code, setCode] = useState("");

  const generateCode = () => {
    const generatedCode = generateRandomCode();
    setCode(generatedCode);
    onChange({ target: { name, value: generatedCode } });
  };

  return (
    <Field name={name}>
      <Stack spacing={1}>
        <FieldLabel>{name}</FieldLabel>
        <Stack horizontal spacing={1}>
          <FieldInput
            type="text"
            placeholder="Generation code"
            value={code || value}
            onChange={() =>
              onChange({
                target: { name, value: e.target.value, type: attribute.type },
              })
            }
          />
          <Button onClick={generateCode}>Generate</Button>
        </Stack>
      </Stack>
    </Field>
  );
};

export default RandomCode;
