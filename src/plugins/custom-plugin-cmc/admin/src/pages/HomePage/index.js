/*
 *
 * HomePage
 *
 */

import React, { useState } from "react";
// import PropTypes from 'prop-types';
import {
  BaseHeaderLayout,
  Box,
  Breadcrumbs,
  Button,
  ContentLayout,
  Crumb,
  Field,
  FieldLabel,
  Stack,
  Textarea,
} from "@strapi/design-system";

import { JSEncrypt } from "jsencrypt";

const HomePage = () => {
  const [rawText, setRawText] = useState("");
  const [publicKey, setPublicKey] = useState("");
  const [encrypted, setEncrypted] = useState("");

  const [encrtText, setEncrtText] = useState("");
  const [privateKey, setPrivateKey] = useState("");
  const [decrtText, setDecrtText] = useState("");

  const handleEncryptClick = () => {
    try {
      const key = new JSEncrypt();
      key.setPublicKey(publicKey);
      const encrypted = key.encrypt(rawText);
      setEncrypted(encrypted);
    } catch (err) {
      console.log(err);
    }
  };

  const handleDecryptClick = () => {
    try {
      const key = new JSEncrypt();
      key.setPrivateKey(privateKey);
      const decrypted = key.decrypt(encrtText);
      setDecrtText(decrypted);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div style={{ width: "50%", padding: "1rem" }}>
      <Box background="neutral100">
        <BaseHeaderLayout
          title="RSA Crypto"
          subtitle={
            <Breadcrumbs label="folders">
              <Crumb>Crypto</Crumb>
            </Breadcrumbs>
          }
          as="h2"
        />
      </Box>
      <ContentLayout>
        <div style={{ paddingBottom: 30 }}>
          <Field name="publicKey">
            <Stack spacing={3}>
              <FieldLabel>Public Key</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="publicKey"
                  type="text"
                  placeholder="Public key (2048)"
                  value={publicKey || ""}
                  onChange={(e) => setPublicKey(e.target.value)}
                />
              </Stack>
            </Stack>
          </Field>
          <Field name="rawText" style={{ margin: "20px auto 20px auto" }}>
            <Stack spacing={3} vertical>
              <FieldLabel>Text to encrypt</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="rawText"
                  type="text"
                  placeholder="Text to encrypt"
                  value={rawText || ""}
                  onChange={(e) => setRawText(e.target.value)}
                />
                <Button
                  style={{ width: "100px", textAlign: "center" }}
                  onClick={handleEncryptClick}
                >
                  Encrypt
                </Button>
              </Stack>
            </Stack>
          </Field>
          <Field>
            <Stack spacing={3} vertical>
              <FieldLabel>Encrypted Text</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="encrypted"
                  type="text"
                  placeholder="Encrypted text"
                  value={encrypted || ""}
                  disabled
                />
              </Stack>
            </Stack>
          </Field>
        </div>
        <div style={{ borderTop: "3px solid #000000", paddingTop: 30 }}>
          <Field name="encrypted">
            <Stack spacing={3}>
              <FieldLabel>Encrypted Text</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="encrypted"
                  type="text"
                  placeholder="Encrypted text"
                  value={encrtText || ""}
                  onChange={(e) => setEncrtText(e.target.value)}
                />
              </Stack>
            </Stack>
          </Field>
          <Field name="privateKey" style={{ margin: "20px auto 20px auto" }}>
            <Stack spacing={3} vertical>
              <FieldLabel>Private key (2048)</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="privateKey"
                  type="text"
                  placeholder="Encrypted text"
                  value={privateKey || ""}
                  onChange={(e) => setPrivateKey(e.target.value)}
                />
                <Button
                  style={{ width: "100px", textAlign: "center" }}
                  onClick={handleDecryptClick}
                >
                  Decrypt
                </Button>
              </Stack>
            </Stack>
          </Field>
          <Field>
            <Stack spacing={3} vertical>
              <FieldLabel>Decrypted Text</FieldLabel>
              <Stack vertical spacing={1}>
                <Textarea
                  name="decrtText"
                  type="text"
                  placeholder="Encrypted text"
                  value={decrtText || ""}
                  disabled
                />
              </Stack>
            </Stack>
          </Field>
        </div>
      </ContentLayout>
    </div>
  );
};

export default HomePage;
