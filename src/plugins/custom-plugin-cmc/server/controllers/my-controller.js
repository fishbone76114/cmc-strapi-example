'use strict';

module.exports = ({ strapi }) => ({
  index(ctx) {
    ctx.body = strapi
      .plugin('custom-plugin-cmc')
      .service('myService')
      .getWelcomeMessage();
  },
});
