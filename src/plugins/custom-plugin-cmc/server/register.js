"use strict";

module.exports = ({ strapi }) => {
  // registeration phase
  strapi.customFields.register({
    name: "code",
    plugin: "custom-plugin-cmc",
    type: "string",
  });
};
